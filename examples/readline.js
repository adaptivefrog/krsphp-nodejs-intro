
//Require readline module
const readline = require('readline');

//Create readline interface
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

//Get an input from commandline
rl.question('What do you think of Node.js? ', (answer) => {
  console.log();
  rl.write(`PHP Rules, but Nodejs also rules! You said: ${answer}`);
  rl.close();
});