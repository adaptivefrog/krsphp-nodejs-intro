var PORT = 41234;
var HOST = '127.0.0.1';

//Udp module
var dgram = require('dgram');

//A mesage to send - in udp it needs to be a buffer
var message = new Buffer('Kristiansand PHP rules');

//We create a client
var client = dgram.createSocket('udp4');

//We send the message
client.send(message, 0, message.length, PORT, HOST, function(err, bytes) {
    if (err) throw err;
    console.log('UDP message sent to ' + HOST +':'+ PORT);
    client.close();
});