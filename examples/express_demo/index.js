
var express = require('express')

var app = express();


app.get('/', function (req, res) {
  res.send('Hello World!')
});

app.get('/demo', function (req, res) {
  res.send('Hello demo World!')
});

app.get('/demo/:item', function (req, res) {
  res.send('Hello demo World with item: ' + req.params['item']);
});

app.post('/demo/:item', function (req, res) {
  res.send('Got a POST request:',req.params);
});

app.put('/demo/:item', function (req, res) {
	console.log('Got a PUT request at /demo',req.params['item']);
  res.send('OK');
})

app.delete('/demo/:item', function (req, res) {
  res.send('Got a DELETE request at /demo',req.params['item']);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
});