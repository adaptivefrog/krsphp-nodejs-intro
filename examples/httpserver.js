//require the http server module
var http = require('http');

//require the filesystem module
var fs = require('fs');

//Read the file to content in a syncronized way
var index = fs.readFileSync('index.html');

//Create a connectionHandler
var connHandler = function (req, res) {
	console.log('Got a request in main handler');
  	//Write header
  	res.writeHead(200, {'Content-Type': 'text/html'});
  	//Write body and end request
  	res.end(index);
  	console.log('Ended request');
};
//Create server
var server = http.createServer(connHandler);

//listener - on request
server.on('request',function(req,resp){
	console.log('Got a request',req.url);
});

//Start server - listen on port 9615
server.listen(9615);